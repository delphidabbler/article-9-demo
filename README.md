DelphiDabbler Article #9 Demo Project
=====================================

This project contains the demo code that accompanies the article "[How to get notified when the content of the clipboard changes](http://delphidabbler.com/articles?article=9)" from [delphidabbler.com](http://delphidabbler.com).

Bugs
----

Please report any bugs via the delphidabbler.com [contact page](http://delphidabbler.com/contact), mentioning the article number.

Of course you can always fork the repo, fix the bug in a new branch, and submit a pull request.

Questions
---------

I'm happy to answer any questions. Again use the [contact page](http://delphidabbler.com/contact), and mention the article number. But **please**, [read the article](http://delphidabbler.com/articles?article=9) first!

License
-------

With the exception of `.gitignore`, any copyright in the the demo project files is dedicated to the
Public Domain - see http://creativecommons.org/publicdomain/zero/1.0/. Do what you want with it. Absolutely no warranty of any kind.

`.gitignore` is based on Delphi.gitignore from https://github.com/github/gitignore, which is [MIT licensed](https://raw.githubusercontent.com/github/gitignore/master/LICENSE).
